const tenantName = "Gemeente Nijmegen";

const primary = {
	50: "#ffe2f1",
	100: "#ffb1cf",
	200: "#ff7faf",
	300: "#ff4d8f",
	400: "#fe1e6e",
	500: "#e50655",
	600: "#b30042",
	700: "#81002f",
	800: "#4f001c",
	900: "#20000a",
};

const secondary = {
	50: "#e1fdf7",
	100: "#c0f0e6",
	200: "#9ae4d6",
	300: "#74d9c6",
	400: "#51ceb5",
	500: "#38b49c",
	600: "#2a8d79",
	700: "#1b6557",
	800: "#0a3c34",
	900: "#001611",
};

window.branding = {
	tenantName,
	colors: {
		primary,
		secondary,
		red: primary,
		green: secondary,
	},
};
