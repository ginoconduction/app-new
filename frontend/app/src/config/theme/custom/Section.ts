const baseStyle = {
	maxWidth: "100%",
	bg: "white",
	p: 5,
	borderRadius: 10,
};

export default {
	baseStyle,
};