export enum RepeatType {
	Once = "Once",
	Week = "Week",
	Month = "Month",
	Year = "Year"
}