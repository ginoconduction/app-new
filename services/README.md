
### Service Componentes
Huishoudboekje uses independent service components, packages as Docker containers.\
These are:
- [Huishoudboekje Service](huishoudboekje_service/)
- [Organisatie Service](organisatie_service/)
- [Bank Transactie Service](bank_transactie_service/)

Services above are all dependent on the core service
- [Core Service](core_service/)