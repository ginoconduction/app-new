""" Mock objects factories """
from .burger import burger_factory
from .organisatie import organisatie_factory
from .afspraak import afspraak_factory
from .rekening import rekening_factory
from .rekening_burger import rekening_burger_factory
from .rekening_organisatie import rekening_organisatie_factory
from .journaalpost import journaalpost_factory
from .rubriek import rubriek_factory
from .configuratie import configuratie_factory
from .export import export_factory
from .overschrijving import overschrijving_factory
