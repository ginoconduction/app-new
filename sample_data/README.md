# Sample data

## Scenarios

> TODO explain scenario yamls

## Regenerate

To regenerate the sample_data please execute the following command:

```shell
make clean generate
```

and commit the results


## Wat is testdata?

### Voorbeelddata (Scenario's)
* Burgers
* Organisatie
* Configuratie
* Alles wat bij het begin nog niet bekend is.
* Om het product te kunnen demonstreren.
* Om testscenario's uit te proberen

### Seeddata
* Organisaties, Rubrieken (enz?)
* Alles wat je op voorhand al in een applicatie kunt inladen.

#### Junkdata
* Willekeurige invoer om hoeveelheden te testen
